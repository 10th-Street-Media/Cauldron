# Cauldron

Cauldron is designed to make it easier for users to submit tickets when they com across bugs or want new feautures to be included in future versions of a product. By implementing ActivityPub, Cauldron lets Fediverse users create tickets without the need to create yet another account. If they want to comment on a bug, or indicate they have the same problem, they can do so. If they have a solution, they can add that also, and they can do it with their Fediverse account.
